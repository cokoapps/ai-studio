/* eslint-disable no-nested-ternary */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  DeleteOutlined,
  EyeOutlined,
  LeftCircleFilled,
  FileAddOutlined,
} from '@ant-design/icons'
import { truncate } from 'lodash'
import mdIcon from '../../../static/md.svg'
import pdfIcon from '../../../static/pdf.svg'
import docxIcon from '../../../static/docx.svg'
import Each from '../component-ai-assistant/utils/Each'
import { StyledSpinner } from '../component-ai-assistant/PromptsInput'

const fileIcons = {
  md: mdIcon,
  docx: docxIcon,
  pdf: pdfIcon,
}

const Root = styled.div`
  --header-height: 35px;
  z-index: 15;
  border-left: 1px solid #0001;
  background: #eee;
  flex-direction: column;
  height: 100%;
  align-items: center;
  position: relative;
  overflow-y: hidden;
  width: 100%;
`

const ChunkMapRoot = styled.div`
  background-color: #fff;
  display: flex;
  flex-direction: column;
  gap: 5px;
  font-size: 14px;
  border-radius: 5px;
  border: 1px solid #0002;
  box-shadow: 0 0 5px #0002;
  padding: 30px 40px;
  line-height: 1.3;
  margin: 5px;
  width: 450px;

  * {
    font-family: Georgia, 'Times New Roman', Times, serif !important;
  }
  > span {
    display: flex;
    gap: 5px;

    strong {
      color: var(--color-blue);
    }

    > p {
      white-space: pre-line;
      word-break: break-word;
      margin: 0;
    }
  }
`

const FileMapRoot = styled.div`
  align-items: center;
  background-color: #ffffff3b;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  gap: 0;
  height: fit-content;
  border-radius: 10px;
  border: 1px solid #0002;
  box-shadow: 0 0 5px #0002;
  padding: 0 0 10px;
  margin: 5px;
  width: 150px;
  overflow: hidden;
  word-break: break-all;
  transition: transform 0.3s;
  user-select: none;
  > :first-child {
    background-color: #f8f8f8;
    border-bottom: 1px solid #0001;
    display: flex;
    justify-content: flex-end;
    width: 100%;
    gap: 3px;
    padding: 3px 5px;
    margin-bottom: 10px;
    color: #2d5465;
    svg {
      height: 12px;
    }
  }
  > img {
    width: 50px;
    margin-bottom: 10px;
    filter: grayscale(20%);
    opacity: 0.85;
  }
  > span {
    display: flex;
    flex-direction: column;
    gap: 5px;
    align-items: center;

    > p {
      color: var(--color-blue);
      margin: 0;
    }
  }
  &:hover {
    transform: scale(1.05);
  }
`

const FilePickerLabel = styled.label`
  cursor: pointer;
  padding: 0.2rem;
  svg {
    width: 1.5em;
    height: 1.5em;
  }
`

const ChunksPagesContainer = styled.div`
  display: flex;
  gap: 10px;
  margin-left: 75px;
  color: #fff;
  line-height: 1.6;
  font-size: 12px;
`

const Header = styled.div`
  display: flex;
  color: #fffe;
  align-items: center;
  justify-content: space-between;
  margin: 0 auto;
  background-color: #f8f8f8;
  padding: 3px 10px 5px;
  width: 100%;
  height: var(--header-height);

  > :first-child > .anticon > svg {
    height: 1.3em;
    width: 1.3em;
  }

  * {
    color: #777;
    margin: 0;
  }

  > p {
    margin: 0;
  }

  > strong {
    padding-left: 15px;
    color: #777;
  }
`

const FilesAndChunksScrollWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: ${p => p.$flexXaxis};
  gap: 12px;
  margin: 0 auto;
  padding: 10px;
  overflow-y: scroll;
  max-height: calc(100% - var(--header-height));
`

const Spinner = styled(StyledSpinner)`
  width: 12px;
  height: 12px;

  &::after {
    border-width: 1px;
    width: 8px;
    height: 8px;
  }
`

export const FilePicker = ({ customLabel, id = 'add-file-to-kb', ...rest }) => (
  <>
    <input
      accept=".md,.pdf,.docx,.html"
      id={id}
      style={{ display: 'none' }}
      type="file"
      {...rest}
    />
    {customLabel ?? (
      <FilePickerLabel htmlFor={id}>
        <FileAddOutlined />
      </FilePickerLabel>
    )}
  </>
)

const FilesMap = ({ documents, remove, select }) => {
  return (
    <Each
      condition={documents?.length > 0}
      fallback={
        <p style={{ color: '#777', textAlign: 'center', width: '100%' }}>
          Upload your first document
        </p>
      }
      of={documents || []}
      render={({ id, extension, name, sectionsKeys, pendingFileExtension }) =>
        !pendingFileExtension ? (
          <FileMapRoot
            onClick={_e => select(id, sectionsKeys?.length, 0, name)}
            title={name}
          >
            <div>
              <DeleteOutlined
                onClick={e => {
                  e.stopPropagation()
                  e.preventDefault()
                  remove({ variables: { id } })
                }}
                title="Delete file"
              />
            </div>
            <img alt={name} src={fileIcons[extension]} />
            <span>
              <p>{truncate(name, { length: 17 })}</p>
              <span>
                <small>Sections: {sectionsKeys?.length}</small>
              </span>
            </span>
          </FileMapRoot>
        ) : (
          <FileMapRoot style={{ filter: 'grayscale(100%)', opacity: 0.5 }}>
            <div>
              <EyeOutlined style={{ opacity: 0 }} />
              <Spinner />
            </div>
            <img
              alt={pendingFileExtension}
              src={fileIcons[pendingFileExtension]}
            />
            <span>
              <p>uploading...</p>
              <span>
                <small> Sections: ?</small>
              </span>
            </span>
          </FileMapRoot>
        )
      }
    />
  )
}

const ChunksMap = ({ loading, data }) => {
  return (
    <Each
      condition={!loading && data?.length > 0}
      fallback={
        <p style={{ color: '#777', textAlign: 'center' }}>
          There is no sections added to the knowledge base
        </p>
      }
      of={[...data]}
      render={files => (
        <ChunkMapRoot>
          <span>
            <p>{files}</p>
          </span>
        </ChunkMapRoot>
      )}
    />
  )
}

export const Files = ({
  createDocument,
  documents,
  getChunks,
  deleteDocument,
  chunksLoading,
}) => {
  const [chunks, setChunks] = useState(null)
  const [docs, setDocs] = useState(false)

  const getChunksData = async (id, keysLng, sectionsPageCurrent, name) => {
    getChunks({
      variables: {
        id,
        start: sectionsPageCurrent ?? 0,
        length: 10,
      },
    }).then(({ data }) => {
      const chunksData = data.getFilesFromDocument
      setChunks({
        id,
        sectionsPageCurrent,
        keysLng,
        chunks: chunksData,
        name,
      })
    })
  }

  const goToPrevPage = _e => {
    const { sectionsPageCurrent, keysLng, id, name } = chunks
    const prev = Math.floor(sectionsPageCurrent - 10)
    const last = Math.floor(keysLng / 10) * 10
    const prevPage = prev < 0 ? last : prev
    getChunksData(id, keysLng, prevPage, name)
  }

  const goToNextPage = _e => {
    const { sectionsPageCurrent, keysLng, id, name } = chunks

    const next = sectionsPageCurrent + 10
    const currentPage = (sectionsPageCurrent / 10) * 1

    const nextPage = currentPage > Math.floor(keysLng / 10) - 1 ? 0 : next

    getChunksData(id, keysLng, nextPage, name)
  }

  const handleFileChange = async e => {
    const file = e.target.files[0]

    if (file) {
      const pendingFileExtension = file.name.split('.').pop()
      setDocs([...documents, { pendingFileExtension }].reverse())
      await createDocument({
        variables: {
          file,
        },
      })
      e.target.value = null
    }
  }

  useEffect(() => {
    documents && setDocs([...documents].reverse())
  }, [documents])

  return (
    <Root>
      <Header>
        {chunks ? (
          <span style={{ display: 'flex', gap: '8px', alignItems: 'center' }}>
            <LeftCircleFilled
              onClick={() => setChunks(null)}
              title="Back to files"
            />
            <p>
              File: {truncate(chunks.name, { length: 30 })}{' '}
              <small>({chunks.keysLng} sections)</small>
            </p>
          </span>
        ) : (
          <strong>UPLOADED FILES</strong>
        )}
        {chunks ? (
          chunks.keysLng > 10 ? (
            <ChunksPagesContainer>
              <ArrowLeftOutlined onClick={goToPrevPage} />
              Page: {(chunks.sectionsPageCurrent / 10) * 1 + 1} of{' '}
              {Math.ceil(chunks.keysLng / 10)}
              <ArrowRightOutlined onClick={goToNextPage} />
            </ChunksPagesContainer>
          ) : (
            ''
          )
        ) : (
          <FilePicker onChange={handleFileChange} title="Upload file" />
        )}
      </Header>
      <FilesAndChunksScrollWrapper
        $flexXaxis={chunks ? 'center' : 'flex-start'}
      >
        {chunks ? (
          <ChunksMap data={chunks?.chunks} loading={chunksLoading} />
        ) : (
          <FilesMap
            documents={docs}
            remove={deleteDocument}
            select={getChunksData}
          />
        )}
      </FilesAndChunksScrollWrapper>
    </Root>
  )
}
