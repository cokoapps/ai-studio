import React, { useContext, useEffect } from 'react'
import styled from 'styled-components'
import { useLazyQuery, useMutation } from '@apollo/client'
import { takeRight } from 'lodash'
import PromptsInput from '../component-ai-assistant/PromptsInput'
import { RAG_SEARCH_QUERY } from '../component-ai-assistant/queries/aiService'
import ChatHistory from '../component-ai-assistant/ChatHistory'
import { AiDesignerContext } from '../component-ai-assistant/hooks/AiDesignerContext'
import logoSmall from '../../../static/AI Design Studio-Icon.svg'

import {
  CREATE_DOCUMENT,
  DELETE_DOCUMENT,
  GET_DOCUMENTS,
  GET_FILES_FROM_DOCUMENT,
} from '../component-ai-assistant/queries/documentAndSections'
import { Files } from './Files'
import { ragSystem } from '../component-ai-assistant/utils'

const Root = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
`

const Container = styled.div`
  background-color: #f5f5f5;
  align-items: center;
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`

const Header = styled.div`
  background-color: #f7f7f7;
  align-items: center;
  box-shadow: #0001 0px 0px 10px 0px;
  justify-content: center;
  display: flex;
  padding: 20px 10px;
  gap: 10px;
  width: 100%;
  z-index: 9;

  > :first-child {
    width: 28px;
    /* left: 24px; */
    margin-top: -7px;
    margin-right: 10px;
    /* position: absolute; */
    filter: drop-shadow(0 0 1px #0005);
  }
`

const Chat = styled(ChatHistory)`
  width: 100%;
  height: fit-content;
  padding-inline: 10%;
  --scrollbar: #0002;
`

const StyledPrompInput = styled(PromptsInput)`
  border-radius: 1.5rem;
  border-color: #8b8b8b44;
  box-shadow: 0 0 4px #0001;
`

const RagTest = () => {
  const {
    userPrompt,
    setUserPrompt,
    setFeedback,
    selectedCtx,
    userImages,
    setSelectedCtx,
    settings,
  } = useContext(AiDesignerContext)

  // #region GQL hooks --------------------------------------------------------------

  const [ragSearchQuery, { loading }] = useLazyQuery(RAG_SEARCH_QUERY, {
    onCompleted: ({ ragSearch }) => {
      const { message = '' } = JSON.parse(ragSearch)

      setFeedback(message.content)
      selectedCtx.history.push({ role: 'assistant', content: message.content })
      setUserPrompt('')
    },
  })

  const [getDocuments, { data: documents }] = useLazyQuery(GET_DOCUMENTS)

  const [createDocument, { loading: creatingDocument }] = useMutation(
    CREATE_DOCUMENT,
    {
      refetchQueries: [GET_DOCUMENTS],
    },
  )

  const [deleteDocument] = useMutation(DELETE_DOCUMENT, {
    refetchQueries: [GET_DOCUMENTS],
  })

  const [getSlicedChunksFromDocument, { loading: slicedChunksLoading }] =
    useLazyQuery(GET_FILES_FROM_DOCUMENT)

  // #endregion GQL hooks ------------------------------------------------------------

  // #region Handlers ---------------------------------------------------------------

  const handleOpenAiQuery = async () => {
    if (userPrompt.length < 2) return
    selectedCtx.history.push({ role: 'user', content: userPrompt })

    const history =
      takeRight(selectedCtx.history, settings.chat.historyMax) || []

    await ragSearchQuery({
      variables: {
        input: {
          text: [userPrompt],
          ...(userImages?.base64Img
            ? { image_url: [userImages?.base64Img] }
            : {}),
        },
        history,
        embeddingOptions: { threshold: 0.9, limit: 20 },
        system: ragSystem,
      },
    })
  }
  // #endregion Handlers ---------------------------------------------------------------

  useEffect(() => {
    setSelectedCtx({ history: [] })
    getDocuments()

    return () => {
      setSelectedCtx(null)
    }
  }, [])

  return (
    <Root>
      <Container>
        <Header $disabled={creatingDocument}>
          <img alt="AiDesignStudioLogo" src={logoSmall} />
          <StyledPrompInput
            disabled={creatingDocument}
            loading={loading}
            onSend={handleOpenAiQuery}
          />
        </Header>
        <Chat nomessages="Talk to your documents" />
      </Container>
      <div style={{ display: 'flex', height: '100%', zIndex: 15 }}>
        <Files
          chunksLoading={slicedChunksLoading}
          createDocument={createDocument}
          deleteDocument={deleteDocument}
          documents={documents?.getDocuments}
          getChunks={getSlicedChunksFromDocument}
        />
      </div>
    </Root>
  )
}

export default RagTest
