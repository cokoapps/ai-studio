import {
  InlineAnnotationsService,
  LinkService,
  ListsService,
  MathService,
  SpecialCharactersService,
  DisplayBlockLevelService,
  TextBlockLevelService,
  BlockDropDownToolGroupService,
} from 'wax-prosemirror-services'

import AiStudioSchema from './waxSchema'
import addAidctxPlugin from './addAidCtxPlugin'

const waxConfig = {
  MenuService: [
    {
      templateArea: 'topBar',
      toolGroups: [
        {
          name: 'Display',
          include: ['Title', 'Heading2', 'Heading3'],
        },
        {
          name: 'Text',
          include: ['Paragraph'],
        },
        {
          name: 'Annotations',
        },
        {
          name: 'Lists',
          exclude: ['JoinUp', 'Lift'],
        },
      ],
    },
  ],

  SchemaService: AiStudioSchema,

  ImageService: { showAlt: true },

  services: [
    new LinkService(),
    new ListsService(),
    new InlineAnnotationsService(),
    new MathService(),
    new SpecialCharactersService(),
    new DisplayBlockLevelService(),
    new TextBlockLevelService(),
    new BlockDropDownToolGroupService(),
  ],

  PmPlugins: [addAidctxPlugin()],
}

export default waxConfig
