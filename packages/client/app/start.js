import { startClient } from '@coko/client'

import routes from './routes'
import theme from './theme'

const options = {}

startClient(routes, theme, options)
