import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import { AiDesignerProvider } from './ui/component-ai-assistant/hooks/AiDesignerContext'
import GlobalStyles from './globalStyles'
import {
  initialPagedJSCSS,
  loadFromLs,
} from './ui/component-ai-assistant/utils'
import AiDesignStudioPage from './pages/AiDesignStudio.page'
import WaxDesignerPage from './pages/WaxDesigner.page'
import HomePage from './pages/Home.Page'

// TODO: Remove when db is set up
const passedSettings = loadFromLs('storedsession')?.settings

const Designer = () => (
  <AiDesignStudioPage
    passedCss={initialPagedJSCSS}
    passedSettings={passedSettings}
  />
)

const WaxDesigner = () => (
  <WaxDesignerPage
    passedCss={initialPagedJSCSS}
    passedSettings={passedSettings}
  />
)

const routes = (
  <AiDesignerProvider>
    <GlobalStyles />
    <Switch>
      <Route component={HomePage} path="/home" />
      <Route component={Designer} path="/ai-designer" />
      <Route component={WaxDesigner} path="/wax-ai-designer" />
      <Redirect from="/" to="/home" />
    </Switch>
  </AiDesignerProvider>
)

export default routes
