/* stylelint-disable no-eol-whitespace */
/* stylelint-disable declaration-no-important */
import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
  #root {
    --color-yellow: #FBCD55;
    --color-yellow-dark: #a27400;
    --color-orange: #FE7B4D;
    --color-orange-dark: #9c4b2e;
    --color-green: #6FAB6A;
    --color-green-dark: #558151;
    --color-blue: #21799E;
    --color-blue-alpha: #21799e52;
    --color-blue-alpha-plus: #21799e0f;
    --color-blue-dark: #154a61;
    --color-fill: #50737c;
    --color-fill-1: #6a919b;
    --color-fill-2: #fff;
    --scrollbar: #004a5c48;
    --color-background-fallback: #fff;
    height: 100dvh;
  }

  body,
  html
  {
    box-sizing: border-box;
    margin: 0;
    line-height: 1;
    padding: 0;
    overflow: hidden;
  }

  button, input, optgroup, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}

  * :not(div#assistant-ctx, .ProseMirror) {
    box-sizing: inherit;
    font-family: Arial, Helvetica, sans-serif;

    ::-webkit-scrollbar {
      height: 5px;
      width: 5px;
    }

    ::-webkit-scrollbar-thumb {
      background: var(--scrollbar);
      border-radius: 5px;
      width: 5px;
    }

    ::-webkit-scrollbar-track {
      background: #fff0;
      padding: 5px;
    
  }
  }
  div#assistant-ctx, .Prosemirror * {
    font-family: var(--font-family);
  }
`
