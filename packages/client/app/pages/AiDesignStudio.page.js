/* eslint-disable react/prop-types */
/* stylelint-disable no-descending-specificity */
// #region IMPORTS ----------------------------------------------------------------
import React, { useContext, useEffect, useRef, useState } from 'react'
import styled, { keyframes } from 'styled-components'
import {
  DeleteOutlined,
  DownOutlined,
  FileSyncOutlined,
  PrinterOutlined,
} from '@ant-design/icons'

import { debounce, takeRight } from 'lodash'
import { useApolloClient, useLazyQuery, useMutation } from '@apollo/client'
import PromptsInput from '../ui/component-ai-assistant/PromptsInput'
import {
  srcdoc,
  initialPagedJSCSS,
  htmlTagNames,
  cssTemplate1,
  setScrollFromPercent,
  getScrollPercent,
  addElement,
  finishReasons,
  AiDesignerSystem,
  snippetsToCssText,
  callOn,
  SnippetIcon,
  getNodes,
  ModelsList,
} from '../ui/component-ai-assistant/utils'
import SelectionBox from '../ui/component-ai-assistant/SelectionBox'
import { AiDesignerContext } from '../ui/component-ai-assistant/hooks/AiDesignerContext'
import ChatBubble from '../ui/component-ai-assistant/ChatBubble'
import ChatHistory from '../ui/component-ai-assistant/ChatHistory'
import SettingsMenu from '../ui/component-ai-assistant/components/SettingsMenu'
import { Manage } from '../ui/component-ai-assistant/components/Management'
import {
  GENERATE_IMAGES,
  CALL_AI_SERVICE,
  GET_IMAGES_URL,
  GET_IMAGE_URL,
  RAG_SEARCH_QUERY,
} from '../ui/component-ai-assistant/queries/aiService'
import {
  GET_SETTINGS,
  UPDATE_SETTINGS,
} from '../ui/component-ai-assistant/queries/settings'
import {
  CREATE_DOCUMENT,
  DELETE_DOCUMENT,
  GET_DOCUMENTS,
  GET_FILES_FROM_DOCUMENT,
} from '../ui/component-ai-assistant/queries/documentAndSections'
import Editor from '../ui/component-ai-assistant/components/Editor'
// #endregion IMPORTS ----------------------------------------------------------------

// #region STYLEDS
const Assistant = styled(PromptsInput)`
  border-bottom: none;
  border-radius: 0;
  border-top: none;
  margin: 10px 0;
  padding: 0 10px;
  width: 480px;

  svg {
    height: 15px;
    width: 15px;
  }
`

const editorLoadingAnim = keyframes`
  0% {
    opacity: 1;
  }

  50% {
    opacity: 0.5;
  }

  100% {
    opacity: 1;
  }
`

const CssAssistantUi = styled.div`
  align-items: center;
  display: flex;
  gap: 1rem;
  justify-content: space-between;
  padding: 0 5px;

  > :last-child {
    align-items: center;
    color: #00495c;
    display: flex;
    gap: 0;

    svg {
      color: var(--color-blue);
      height: 15px;
      width: 15px;
      transition: all 0.3s;
    }
    span.anticon {
      padding: 1px 5px;
    }
    span[data-inactive='true'] svg {
      color: #bbb;
    }
    span[data-modelicon='true'] svg {
      width: 8px;
      height: 8px;
    }
    > :last-child {
      cursor: pointer;
      margin-left: 0.3rem;
    }
  }
`

const StyledHeading = styled.div`
  --snippet-icon: var(--color-blue);
  --snippet-icon-st: #fff;

  align-items: center;
  background-color: #fff;
  border-bottom: 1px solid #0004;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 0 0 0 10px;
  position: relative;
  scrollbar-color: #00495c;
  scrollbar-width: thin;
  width: 100%;
  z-index: 999999999;

  button > svg {
    color: var(--color-blue);
    height: 20px;
    width: 20px;
  }
`

const Root = styled.div`
  --color-yellow: #fbcd55;
  --color-yellow-dark: #a27400;
  --color-orange: #fe7b4d;
  --color-orange-dark: #9c4b2e;
  --color-green: #6fab6a;
  --color-green-dark: #558151;
  --color-blue: #21799e;
  --color-blue-dark: #154a61;
  --color-fill: #50737c;
  --color-fill-1: #6a919b;
  --color-fill-2: #fff;
  --color-disabled: #ccc;
  --color-enabled: #21799e;
  --color-yellow-alpha-1: #fbcd55aa;
  --color-orange-alpha-1: #fe7b4daa;
  --color-green-alpha-1: #6fab6aaa;
  --color-blue-alpha-1: #21799eaa;
  --color-yellow-alpha-2: #fbcd5511;
  --color-orange-alpha-2: #fe7b4d11;
  --color-green-alpha-2: #6fab6a11;
  --color-blue-alpha-2: #21799e11;

  border: 1px solid #0002;
  border-radius: 0 8px 8px;
  display: flex;
  flex-direction: column;
  height: 100vh;
  overflow: hidden;
  position: relative;
  width: 100%;

  * {
    ::-webkit-scrollbar {
      height: 5px;
      width: 5px;
    }

    ::-webkit-scrollbar-thumb {
      background: #004a5c48;
      border-radius: 5px;
      width: 5px;
    }

    ::-webkit-scrollbar-track {
      background: #fff0;
      padding: 5px;
    }
  }
`

const EditorContainer = styled.div`
  background: whitesmoke;
  display: flex;
  justify-content: ${p => (!p.$alignX ? 'center' : 'flex-start')};
  filter: ${p => (p.$loading ? 'blur(2px)' : '')};
  height: calc(100vh - 70px);
  overflow: auto;
  padding: 40px;
  position: relative;
  scroll-behavior: smooth;
  transition: width 0.5s;
  user-select: none;

  ::-webkit-scrollbar {
    height: 5px;
    width: 5px;
  }

  ::-webkit-scrollbar-thumb {
    background: #00495c;
    border-radius: 5px;
    width: 5px;
  }

  ::-webkit-scrollbar-track {
    background: #fff0;
    padding: 5px;
  }
`

const PreviewIframe = styled.iframe`
  border: none;
  display: flex;
  height: calc(100vh - 10px);

  width: 100%;
`

const CheckBoxes = styled.div`
  align-items: center;
  border-left: 1px solid #0002;
  color: #555;
  display: flex;
  font-size: 14px;
  line-height: 1.3;
  padding: 0 0.7rem 0 0;
  position: relative;

  > span {
    height: fit-content;
    padding: 5px 10px;
  }
`

const WindowsContainer = styled.div`
  background: #eee;
  display: flex;
  height: calc(100%);
  position: relative;
  width: 100%;
`

const StyledWindow = styled.div`
  display: flex;
  flex-direction: column;
  height: calc(100vh - 47px);
  overflow: hidden;
  position: relative;
  transition: width 0.5s ease;
  width: ${p => (p.$show ? '100%' : '0')};
`

const WindowHeading = styled.div`
  align-items: center;
  background-color: #efefef;
  box-shadow: inset 0 0 5px #fff4, 0 0 2px #0009;
  color: #777;
  display: flex;
  font-size: 12px;
  font-weight: bold;
  justify-content: space-between;
  line-height: 1;
  min-height: 23px;
  padding: 2px 10px;
  white-space: nowrap;
  z-index: 99;

  svg {
    fill: #00495c;
    stroke: #00495c;
  }

  > :first-child {
    color: #aaa;
  }
`

const WindowDivision = styled.div`
  background-color: #fff;
  border: 1px solid #0004;
  border-top-color: #fff;
  height: calc(100% + 1px);
  margin-top: -1px;
  width: 8px;
  z-index: 999999;
`

const LoadingOverlay = styled.div`
  height: 100%;
  position: absolute;
  width: 100%;
  z-index: 9999;
`

const OverlayAnimated = styled(LoadingOverlay)`
  align-items: center;
  background: #fff6;
  display: flex;
  justify-content: center;
  opacity: ${p => (p.$loading ? 1 : 0)};
  pointer-events: none;
  transition: opacity 1.5s;

  > span {
    animation: ${p => (p.$loading ? editorLoadingAnim : 'none')} 1.5s infinite;
    color: #00495c;
    font-size: 40px;
  }
`

// #endregion STYLEDS
const voidElements = [
  'area',
  'base',
  'br',
  'col',
  'embed',
  'hr',
  'img',
  'input',
  'link',
  'meta',
  'param',
  'source',
  'track',
  'wbr',
]

const Dropdown = styled.div`
  position: absolute;
  background: #fff;
  min-width: 100px;
  height: fit-content;
  top: calc(100% + 1px);
  border: 1px solid ${p => (p.$open ? 'var(--color-blue-alpha)' : '#0000')};
  border-top: none;
  left: 0;
  max-height: ${p => (p.$open ? '200px' : '0')};
  transition: all 0.5s;
  overflow: hidden;

  ul {
    list-style: none;
    margin: 0;
    padding: 0;
    display: flex;
    flex-direction: column;
    small {
      background-color: var(--color-blue);
      color: #f1f1f1;
      width: 100%;
      padding: 2px 4px;
    }
    li {
      display: flex;
      align-items: center;
      margin: 0;
      padding: 8px 10px;
      height: 25px;
      button {
        display: flex;
        align-items: center;
        justify-content: space-between;
        font-size: 11px;
        color: #999;
        margin: 0;
        padding: 0;
        width: 100%;
        background: none;
        border: none;
        outline: none;
        cursor: pointer;
        text-align: left;
        &::after {
          display: flex;
          content: ' ';
          width: 5px;
          height: 5px;
          background-color: #bbb;
          border-radius: 50%;
        }
      }
      button[data-selected='true'] {
        color: var(--color-blue);
        &::after {
          background-color: var(--color-blue);
        }
      }
      &:hover {
        background-color: var(--color-blue-alpha-2);
      }
    }
    li:not(:last-child) {
      border-bottom: 1px solid #0001;
    }
  }
`

const ModelsDropdown = ({ model, setModel }) => {
  const [openDropdown, setOpenDropdown] = useState(false)
  const handleOpen = () => setOpenDropdown(!openDropdown)

  return (
    <span
      style={{
        display: 'flex',
        alignItems: 'center',
        fontSize: '12px',
        borderBottom: '1px solid var(--color-blue-alpha)',
        padding: '3px 3px 3px 8px',
        gap: '0.5rem',
        position: 'relative',
        minWidth: '100px',
        justifyContent: 'space-between',
      }}
    >
      <button
        onClick={handleOpen}
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          background: 'none',
          margin: '0',
          outline: 'none',
          padding: '0',
          border: 'none',
          width: '100%',
          height: '100%',
          cursor: 'pointer',
          color: 'var(--color-blue)',
        }}
        type="button"
      >
        <small>{model[2]}</small>
        <DownOutlined data-modelicon label={model[2]} title="Select model" />
      </button>
      <Dropdown $open={openDropdown}>
        {Object.entries(ModelsList).map(([api, list]) => (
          <ul key={api}>
            <small>{api}</small>
            {list.map(({ model: mod, label }) => {
              const handleSetModel = () => {
                setModel([api, mod, label])
                setOpenDropdown(false)
              }

              return (
                <li key={label}>
                  <button
                    data-selected={model[2] === label}
                    onClick={handleSetModel}
                    type="button"
                  >
                    {label}
                  </button>
                </li>
              )
            })}
          </ul>
        ))}
      </Dropdown>
    </span>
  )
}

const AiDesignStudioPage = ({ bookTitle, passedSettings }) => {
  // #region REACT Hooks ----------------------------------------------------------------
  const {
    css,
    htmlSrc,
    setSelectedCtx,
    setSelectedNode,
    selectedCtx,
    editorContent,
    updateSelectionBoxPosition,
    styleSheetRef,
    setCss,
    selectedNode,
    setFeedback,
    setUserPrompt,
    addSnippet,
    onHistory,
    history,
    clearHistory,
    updateCtxNodes,
    userPrompt,
    getCtxBy,
    settings,
    setSettings,
    markedSnippet,
    saveSession,
    userImages,
  } = useContext(AiDesignerContext)

  const previewScrollTopRef = useRef(0)
  const previewRef = useRef(null)
  const editorContainerRef = useRef(null)
  const [previewSource, setPreviewSource] = useState(null)
  const [livePreview, setLivePreview] = useState(true)
  const [showEditor, setShowEditor] = useState(true)
  const [showPreview, setShowPreview] = useState(true)
  const [showChat, setShowChat] = useState(false)
  const [showSettings, setShowSettings] = useState(false)
  const [showSnippetsWindow, setShowSnippetsWindow] = useState(false)
  const [useRag, setUseRag] = useState(false)
  const [model, setModel] = useState(['openAi', 'gpt-4o', 'GPT-4o'])
  // #endregion REACT Hooks ----------------------------------------------------------------

  // #region GQL Hooks ----------------------------------------------------------------

  const client = useApolloClient()

  // const currentUser = useCurrentUser()

  const [getSettings] = useLazyQuery(GET_SETTINGS)

  const [updateSettings] = useMutation(UPDATE_SETTINGS)

  const [getGeneratedImages, { data: imageUrls }] = useLazyQuery(
    GET_IMAGES_URL,
    {
      variables: { size: 'medium' },
    },
  )

  const [generateImages, { loading: dalleLoading }] =
    useLazyQuery(GENERATE_IMAGES)

  const [callAiService, { loading, error }] = useLazyQuery(CALL_AI_SERVICE, {
    onCompleted: async ({ aiService }) => {
      const { message = '', finish_reason: finishReason = '' } =
        JSON.parse(aiService)

      if (message.content.startsWith('{')) {
        try {
          const response = JSON.parse(message.content)

          const {
            css: resCss,
            snippet,
            feedback,
            content = '',
            insertHtml,
            addTibetan,
            callDallE,
          } = response

          if (resCss || insertHtml || snippet || content || addTibetan) {
            onHistory.addRegistry('undo')
            history.current.source.redo = []
          }

          const isSE = selectedCtx.node !== htmlSrc

          if (snippet && isSE) {
            addSnippet(selectedCtx.node, snippet)
          } else if (resCss) {
            styleSheetRef.current.textContent = resCss
            setCss(styleSheetRef.current.textContent)
          }

          insertHtml &&
            addElement(selectedNode, {
              ...insertHtml,
              position: !voidElements.includes(selectedNode.localName)
                ? insertHtml.position
                : 'afterend',
            })

          if (callDallE) {
            await generateImages({
              variables: { input: callDallE },
            }).then(({ data: { generateImages: aiImages } }) => {
              isSE &&
                addElement(selectedNode, {
                  html: `<img class="aid-snip-img-default" src="${aiImages.s3url}" data-imgkey="${aiImages.imageKey}" />`,
                })
              client.refetchQueries({
                include: [GET_IMAGES_URL],
              })
            })
          }

          /**
           addTibetan &&
             isSE
               ? htmlSrc.querySelectorAll('.tibetan').forEach(
                   el =>
                     ![...htmlSrc.querySelectorAll('*')]
                       .map(e => e.textContent)
                       .includes(convertTibetanToENphonethics(el.textContent)) &&
                     addElement(el, {
                       html: `<p class="aid-snip-tibetan-to-phonetics">${convertTibetanToENphonethics(
                         el.textContent,
                       )}</p>`,
                     }),
                 )
               : addElement(selectedNode, {
                   html: `<p class="aid-snip-tibetan-to-phonetics">${convertTibetanToENphonethics(
                     selectedNode.textContent,
                   )}</p>`,
                 }))
           */

          content && (selectedCtx.node.innerHTML = content)
          feedback && setFeedback(feedback)
          feedback &&
            selectedCtx.history.push({ role: 'assistant', content: feedback })

          updatePreview(true)
        } catch (err) {
          if (finishReasons[finishReason]) {
            setFeedback(finishReasons[finishReason])
            selectedCtx.history.push({
              role: 'assistant',
              content: finishReasons[finishReason],
            })
          }
        }
      } else {
        setFeedback(
          'There was an error generating the response\n Please, try again in a few seconds',
        )
      }

      setUserPrompt('')
    },
  })

  const [getImageUrl] = useLazyQuery(GET_IMAGE_URL)

  const [getDocuments, { data: documents }] = useLazyQuery(GET_DOCUMENTS)

  const [createDocument] = useMutation(CREATE_DOCUMENT, {
    refetchQueries: [GET_DOCUMENTS],
  })

  const [deleteDocument] = useMutation(DELETE_DOCUMENT, {
    refetchQueries: [GET_DOCUMENTS],
  })

  const [getSlicedChunksFromDocument, { loading: slicedChunksLoading }] =
    useLazyQuery(GET_FILES_FROM_DOCUMENT)

  const [ragSearchQuery, { loading: ragSearchLoading }] =
    useLazyQuery(RAG_SEARCH_QUERY)

  // #endregion GQL Hooks ----------------------------------------------------------------

  // #region EFFECTS -----------------------------------------------------------------------
  useEffect(() => {
    getDocuments()
    window.addEventListener('keydown', handleKeyboard)

    return () => {
      window.removeEventListener('keydown', handleKeyboard)
    }
  }, [])

  useEffect(() => {
    showPreview && livePreview && updatePreview()
    if (css) styleSheetRef.current.textContent = css
  }, [htmlSrc, css, editorContent])

  useEffect(() => {
    showPreview && updatePreview()
    !showPreview && !showEditor && setShowEditor(true)
  }, [showPreview])

  useEffect(() => {
    if (!showEditor) {
      setSelectedCtx(getCtxBy('node', htmlSrc))
      setSelectedNode(htmlSrc)
      !showPreview && setShowPreview(true)
    }

    updatePreview()
  }, [showEditor])

  useEffect(() => {
    error && setFeedback(error.message)
  }, [error])
  useEffect(() => {
    const { Icons, ...restSettings } = settings
    getGeneratedImages()
    settings &&
      updateSettings({
        variables: {
          settings: { ...restSettings },
        },
      }).then(() => getSettings())
  }, [])

  // #endregion EFFECTS -----------------------------------------------------------------------

  // #region HANDLERS & HELPERS ----------------------------------------------------------------

  const handleKeyboard = e => {
    const { key, ctrlKey } = e

    const ctrlKeyHandlers = {
      m: () => {
        setShowSnippetsWindow(prev => !prev)
      },
      e: () => {
        e.preventDefault()
        setSettings(prev => ({
          ...prev,
          editor: {
            ...prev.editor,
            contentEditable: !prev.editor.contentEditable,
          },
        }))
      },
      s: () => {
        e.preventDefault()
        saveSession()
      },
      z: () => {
        onHistory.apply('undo')
      },
      y: () => {
        onHistory.apply('redo')
      },
    }

    callOn(key, {
      Escape: ev => {
        if (!showSnippetsWindow) return
        ev.preventDefault()
        setShowSnippetsWindow(false)
      },
      ...(ctrlKey ? ctrlKeyHandlers : {}),
    })
  }

  const handleScroll = e => {
    const iframeElement = previewRef?.current?.contentDocument?.documentElement
    if (!iframeElement) return
    const percentage = Math.round(getScrollPercent(e.target))
    iframeElement.scrollTo(0, setScrollFromPercent(iframeElement, percentage))
  }

  const handleSend = async e => {
    if (loading || userPrompt?.length < 2 || !selectedCtx?.node) return
    e?.preventDefault()

    const input = {
      text: [userPrompt],
      ...(userImages?.base64Img ? { image_url: [userImages.base64Img] } : {}),
    }

    const clampedHistory =
      takeRight(selectedCtx.history, settings.chat.historyMax) || []

    const systemPayload = {
      ctx: selectedCtx ?? getCtxBy('node', htmlSrc),
      sheet: styleSheetRef?.current?.textContent,
      selectors: getNodes(htmlSrc, '*', 'localName'),
      providedText: selectedNode !== htmlSrc && selectedCtx.node.innerHTML,
      markedSnippet,
      snippets: settings.snippetsManager.snippets,
    }

    const system = AiDesignerSystem(systemPayload)

    if (useRag) {
      const {
        data: { ragSearch },
      } = await ragSearchQuery({
        variables: {
          input,
          embeddingOptions: { threshold: 0.9, limit: 20 },
          resultsOnly: true,
        },
      })

      const embeddingsContent = JSON.parse(ragSearch).join('\n')

      system.task += `\n\t- 'user' may provide some document fragments to contextualize its queries, you will see that on the context block`
      system.context += `\n\t- This is the content retrieved from the documents: \nBLOCK DOCUMENTS:\n${embeddingsContent}\nEND BLOCK DOCUMENTS`
    }

    callAiService({
      variables: {
        input,
        system,
        history: clampedHistory,
        model,
      },
    })

    selectedCtx.history.push({ role: 'user', content: userPrompt })
  }

  const updatePreview = debounce(manualUpdate => {
    const previewDoc = previewRef?.current?.contentDocument?.documentElement

    previewDoc &&
      previewDoc.scrollTop > 0 &&
      (previewScrollTopRef.current = previewDoc.scrollTop)

    css &&
      htmlSrc?.outerHTML &&
      (livePreview || manualUpdate) &&
      setPreviewSource(
        srcdoc(
          htmlSrc.outerHTML.replace('contenteditable="true"', ''),
          css,
          cssTemplate1 + snippetsToCssText(settings.snippetsManager.snippets),
          previewScrollTopRef.current,
        ),
      )
    updateCtxNodes()
    updateSelectionBoxPosition()
  }, 100)

  const updateImageUrl = async (imagekey, cb) =>
    getImageUrl({ variables: { imagekey } }).then(cb)

  const scrollToSelectedNode = () => {
    selectedCtx?.node &&
      editorContainerRef?.current &&
      editorContainerRef.current.scrollTo(0, selectedCtx.node.offsetTop)
  }

  // #endregion HANDLERS & HELPERS ----------------------------------------------------------------

  // #region COMPONENT -------------------------------------------------------------------------
  return (
    <Root>
      {settings?.snippetsManager?.snippets && (
        <style id="aid-snippets">
          {snippetsToCssText(settings.snippetsManager.snippets)}
        </style>
      )}
      <StyledHeading>
        <CssAssistantUi>
          <ChatBubble forceHide={showChat} onRight />
          <Assistant
            loading={loading || ragSearchLoading || dalleLoading}
            onSend={handleSend}
            placeholder="Type here how your article should look..."
          />
          <span>
            <settings.Icons.UndoIcon
              onClick={() => onHistory.apply('undo')}
              title="Undo (Ctrl + z)"
            />
            <settings.Icons.RedoIcon
              onClick={() => onHistory.apply('redo')}
              title="Redo (Ctrl + y)"
            />
            <settings.Icons.RefreshIcon
              onClick={updatePreview}
              title="Update preview"
              type="button"
            />
            <PrinterOutlined
              as="button"
              onClick={() => previewRef?.current?.contentWindow?.print()}
              title="Print"
              type="button"
            />
            <FileSyncOutlined
              data-inactive={!useRag}
              onClick={() => setUseRag(!useRag)}
              title="Use uploaded documents"
            />
            <ModelsDropdown model={model} setModel={setModel} />
          </span>
        </CssAssistantUi>
        <CheckBoxes>
          <SnippetIcon
            onClick={() => setShowSnippetsWindow(!showSnippetsWindow)}
            title={`${
              !showSnippetsWindow ? 'Open' : 'Close'
            } Snippet Manager (Ctrl + M)`}
          />
          <settings.Icons.SettingsIcon
            onMouseEnter={() => setShowSettings(!showSettings)}
            style={{
              cursor: 'pointer',
            }}
          />
        </CheckBoxes>
      </StyledHeading>
      <SettingsMenu
        livePreview={livePreview}
        onMouseLeave={() => setShowSettings(false)}
        setLivePreview={setLivePreview}
        setShowChat={setShowChat}
        setShowEditor={setShowEditor}
        setShowPreview={setShowPreview}
        showChat={showChat}
        showEditor={showEditor}
        showPreview={showPreview}
        showSettings={showSettings}
        updatePreview={updatePreview}
      />
      <WindowsContainer>
        {showSnippetsWindow && (
          <Manage
            chunksLoading={slicedChunksLoading}
            createDocument={createDocument}
            deleteDocument={deleteDocument}
            documents={documents?.getDocuments}
            getChunks={getSlicedChunksFromDocument}
            getImageUrl={updateImageUrl}
            imagesData={imageUrls}
            setShow={setShowSnippetsWindow}
            updatePreview={updatePreview}
          />
        )}
        <StyledWindow
          $show={showChat}
          style={{ maxWidth: '30%', background: '#f5f5f5' }}
        >
          <WindowHeading>
            <span>CHAT HISTORY</span>
            <DeleteOutlined
              onClick={clearHistory}
              title="Clear history (not undoable)"
            />
          </WindowHeading>
          <ChatHistory />
        </StyledWindow>

        {showChat && (showEditor || showPreview) && <WindowDivision />}

        <StyledWindow $show={showEditor}>
          <WindowHeading>
            <span>
              CONTENT SELECTION{bookTitle ? `for: "${bookTitle}"` : ':'}
            </span>
            <button
              onClick={scrollToSelectedNode}
              style={{
                border: '1px dashed #0004',
                color: '#555',
                cursor: 'pointer',
              }}
              title="Scroll to selected element"
              type="button"
            >
              {selectedCtx?.node && selectedCtx.node !== htmlSrc
                ? htmlTagNames[selectedCtx?.tagName]
                : 'Article'}
            </button>
          </WindowHeading>
          {(loading || dalleLoading) && <LoadingOverlay />}
          <OverlayAnimated $loading={loading || dalleLoading}>
            <span>
              {(loading && 'Processing...') ||
                (ragSearchLoading && 'Querying documents...') ||
                (dalleLoading && 'Generating image...') ||
                ''}
            </span>
          </OverlayAnimated>
          <EditorContainer
            $alignX={!showPreview && showSnippetsWindow}
            $loading={loading}
            onScroll={handleScroll}
            ref={editorContainerRef}
          >
            <Editor
              getImageUrl={updateImageUrl}
              stylesFromSource={initialPagedJSCSS}
              updatePreview={updatePreview}
            />
            <SelectionBox updatePreview={updatePreview} />
          </EditorContainer>
        </StyledWindow>

        {showEditor && showPreview && <WindowDivision />}

        <StyledWindow $show={showPreview}>
          <WindowHeading>
            <span>
              ARTICLE PREVIEW{bookTitle ? ` for: "${bookTitle}"` : ':'}
            </span>
          </WindowHeading>
          <PreviewIframe
            onLoad={updatePreview}
            ref={previewRef}
            srcDoc={previewSource}
            title="Article preview"
          />
        </StyledWindow>
      </WindowsContainer>
    </Root>
  )
  // #endregion COMPONENT -------------------------------------------------------------------------
}

export default AiDesignStudioPage
