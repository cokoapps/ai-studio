import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import AIStudioLogoSquare from '../../static/AI Design Studio-Icon.svg'
import WaxLogo from '../../static/waxdesigner2.svg'

export const FlexCol = styled.div`
  display: flex;
  flex-direction: column;
  text-decoration: none;
  font-size: 18px;
  color: #888;

  img {
    height: 180px;
    object-fit: contain;
  }
`

export const StyledLink = styled(Link)`
  * {
    filter: drop-shadow(0px 0px 4px #00000011);
  }
  background-color: #fff;
  text-decoration: none;
  color: var(--color-blue);
  padding: 2rem 3.5rem 0.5rem 3rem;
  border-radius: 1rem;
  border: 1px solid #0004;
  opacity: 0.9;
  box-shadow: 0 0 8px #0002;
  transition: transform 0.4s, opacity 0.4s;

  &:hover {
    opacity: 1;
    transform: scale(1.05);
  }
`

const HomePage = () => {
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        gap: '40px',
        background: '#f5f5f5',
      }}
    >
      <StyledLink to="/ai-designer">
        <FlexCol>
          <img alt="Ai Design Studio" src={AIStudioLogoSquare} />
          <p
            style={{
              alignSelf: 'center',
              marginTop: '25px',
              paddingLeft: '0.5rem',
            }}
          >
            AI Design Studio
          </p>
        </FlexCol>
      </StyledLink>
      <StyledLink to="/wax-ai-designer">
        <FlexCol>
          <img alt="Wax Ai Design Studio" src={WaxLogo} />
          <p
            style={{
              alignSelf: 'center',
              marginTop: '25px',
              paddingLeft: '0.5rem',
            }}
          >
            Wax AI Design Studio
          </p>
        </FlexCol>
      </StyledLink>
    </div>
  )
}

export default HomePage
