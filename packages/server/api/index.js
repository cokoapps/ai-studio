const { merge } = require('lodash')
const aiService = require('./aiService')
const settings = require('./settings')
const document = require('./document')

module.exports = {
  typeDefs: [aiService.typeDefs, settings.typeDefs, document.typeDefs].join(
    ' ',
  ),
  resolvers: merge(
    {},
    aiService.resolvers,
    settings.resolvers,
    document.resolvers,
  ),
}
