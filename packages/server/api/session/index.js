const fs = require('fs')
const path = require('path')
const resolvers = require('./session.resolvers')

module.exports = {
  resolvers,
  typeDefs: fs.readFileSync(path.join(__dirname, 'session.graphql'), 'utf-8'),
}