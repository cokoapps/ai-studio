module.exports = [
  '@coko/server/src/models/user',
  '@coko/server/src/models/identity',
  '@coko/server/src/models/team',
  '@coko/server/src/models/teamMember',
  '@coko/server/src/models/file',
  './api',
  './models/settings',
  './models/embeddings',
  './models/document',
]
