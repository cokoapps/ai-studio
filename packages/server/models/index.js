const Document = require('./document/document.model')
const Embedding = require('./embeddings/embedding.model')
const Settings = require('./settings/settings.model')

module.exports = {
  Settings,
  Embedding,
  Document,
}
