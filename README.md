# AI Studio

## Setup Instructions

1. **Clone the Repository**: First, clone this repository to your local machine.

2. **API Keys**: To enable the application to function properly, you'll need API keys for both OpenAI and Mistral. Create a `.env` file at the root of the project directory and add the following lines, replacing `'your_openai_api_key'` and `'your_mistral_api_key'` with your actual API keys:

```
OPENAI_API_KEY='your openai_api_key'
MISTRAL_API_KEY='your mistral_api_key'
```

3. **Development Environment**: For development purposes, simply execute the command `docker-compose up` in your terminal. This will start the necessary services defined in the `docker-compose.yml` file.
